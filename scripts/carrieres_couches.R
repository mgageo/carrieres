# <!-- coding: utf-8 -->
#
# la partie couches : vecteur et raster
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
# les cartes pour l'ensemble des sites, la version Tex
# source("geo/scripts/carrieres.R"); couches_jour()
couches_jour <- function(ogc = FALSE, cartes = FALSE, test = 2) {
  carp()
  library(tidyverse)
  library(utf8)
  library(janitor)
  cartesDir <- sprintf("%s/couches_cartes", texDir)
  dir.create(cartesDir, showWarnings = FALSE)
  nc <- couches_osm_lire()
  tex_dftpl2fic(nc, "couches_cartes")
  if (ogc == TRUE) {
    couches_ogc(test = test)
  }
  if (cartes == TRUE) {
    couches_cartes(test = test)
  }
  tex_pdflatex("sites.tex")
}
#
# les couches ogc : récupération sur internet
# source("geo/scripts/carrieres.R");couches_ogc(2)
couches_ogc <- function(test=2) {
  carp()
  library(tidyverse)
  library(sf)
  fonds_images_lire_carrieres()
  dsn <- sprintf("%s/%s.geojson", varDir, "osm")
  carp("dsn: %s", dsn)
  nc <- couches_osm_lire()
#  stop('***')
  ogcDir <<- sprintf("%s/ogc", varDir)
  dir.create(ogcDir, showWarnings = FALSE)
  fonds.df <- couches_fonds()
  for ( i in 1:nrow(nc) ) {
    site <- nc[[i, "site"]]
    carp("i: %s site: %s", i,  site)
    fonds_ogc_fonds(fonds.df, nc[i,], site, test)
#    break
  }
}
#
# production du pdf à partir des images
# setwd("d:/web");source("geo/scripts/carrieres.R"); couches_cartes(1)
couches_cartes <- function(test = 2) {
  carp()
  library(tidyverse)
  cartesDir <- sprintf("%s/couches_cartes", texDir)
  dir.create(cartesDir, showWarnings = FALSE)
  nc <- couches_osm_lire()
  fonds.df <- couches_fonds()
  for (i in 1:nrow(nc)) {
    site <- nc[[i, "site"]]
    couches_cartes_fonds(fonds.df, nc[i, ], site, cartesDir, test = test)
  }
}
#
# les fonctions génériques "fonds"
# =============================================================================
#
# génération des différents pdf
couches_cartes_fonds <- function(df, nc, site, pdfDir, test) {
  carp("site: %s", site)
  for (i in 1:nrow(df)) {
    e <- df[i, "pdf"]
    fond <- df[i, "fond"]
#    test <- df[i, "test"]
    param <- df[i, "param"]
    marge <- df[i, "marge"]
    carp("fond: %s", fond)
    dsn <- sprintf("%s/%s_%s.pdf", pdfDir, site, fond)
    if (test != 1 & file.exists(dsn)) {
      next
    }
#    carp("test: %s dsn: %s", test, dsn)
    fonction <- sprintf("couches_cartes_%s", e)
    if (! exists(fonction, mode = "function")) {
      carp("fonction inconnue: %s", fonction)
      stop("***")
    }
    img <- do.call(fonction, list(nc = nc, site = site, df = df[i, ]))
    dev.copy(pdf, dsn, width = par("din")[1], height = par("din")[2])
    carp("dsn: %s %sx%s", dsn, par("din")[1], height = par("din")[2])
#    stop("***")
    graphics.off()
  }
#  stop("***")
}
#
# le cas d'une image de type brick
couches_cartes_img <- function(nc, site, df, affiche = TRUE) {
  library(raster)
  Carp("site: %s", site)
  dsn <- sprintf(df[1, "param"], varDir, site)
  Carp("dsn: %s", dsn)
  img <- brick(dsn)
  plotImg(img)
  plot(st_geometry(nc), lwd = 8, col = "transparent", add = TRUE)
#  geo_echelle()
  legend("topright", legend = site, cex = 3, text.col = "darkblue", box.lwd = 0, box.col = "transparent", bg = "transparent")
}
#
# la configuration des cartes
couches_fonds <- function(test=2) {
  carp()
  df <- read.table(text="fonction|pdf|fond|param|marge|size_max
crop|img|gmses13|%s/ogc/%s_gmses13.tif|5000|1024
read|img|gb_photo|%s/ogc/%s_gb_photo.tif|300|1024
read|img|gb_ancien|%s/ogc/%s_gb_ancien.tif|300|1024
read|img|carte|%s/ogc/%s_carte.tif|300|1024
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors = FALSE, quote = "") %>%
    filter(!grepl('^#', fonction)) %>%
    glimpse()
  return(invisible(df))
}
couches_osm_lire <- function(test=2) {
  carp()
  library(tidyverse)
  library(sf)
  dsn <- sprintf("%s/%s.geojson", varDir, "osm")
  carp("dsn: %s", dsn)
  nc <- st_read(dsn) %>%
    st_transform(2154) %>%
    mutate(site = sprintf("%02d", no)) %>%
#    filter(site == "23") %>%
    mutate(commune = name) %>%
    glimpse()
  return(invisible(nc))
}